import React from 'react';
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/mode-css";
import "ace-builds/src-noconflict/mode-java";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/mode-ruby";
import "ace-builds/src-noconflict/mode-python";
import "ace-builds/src-noconflict/mode-sass";
import "ace-builds/src-noconflict/mode-typescript";
import "ace-builds/src-noconflict/mode-xml";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-noconflict/ext-language_tools";
import { withStyles } from '@material-ui/core/styles';
import ScreenRotationIcon from '@material-ui/icons/ScreenRotation';
import TonalityIcon from '@material-ui/icons/Tonality';
import MenuIcon from '@material-ui/icons/Menu';
import { getDefaultCode, getNewFileCode } from './libs/DefaultCode';
import DialogUI from './libs/DialogUI';
import Typography from '@material-ui/core/Typography';
import MenuUI from './libs/MenuUI';

const useStyles = theme => ({
    edCont: {
        display: "flex", flexDirection: "column", padding: "8px", backgroundColor: "#f4f4f4"
    },
    headerWra: {
        height: "36px", backgroundColor: "#f4f4f4", display: "flex", padding: "0px"
    },
    icWra: {
        display: "flex", justifyContent: "center", flexDirection: "column", padding: "0px 16px", cursor: "pointer",
        '&:hover': {
            backgroundColor: "#ccc"
        }
    },
    icon: {
        color: "#999999",
        '&:hover': {
            color: "#757575",
        }
    },
    runBtn: {
        color: "#fff", backgroundColor: "#4CAF50", height: "36px", display: "flex", justifyContent: "center", flexDirection: "column", textAlign: "center", width: "72px", cursor: "pointer",
        '&:hover': {
            backgroundColor: "#ccc", color: "#757575"
        }
    },
    editorWrapper: {
        display: "flex"
    },
    aceEdWra: {
        flex: 1, border: "1px solid"
    },
    frameWra: {
        outline: "none", height: 'calc(100vh - 86px)', border: "1px solid", flex: 1
    },
    frame: {
        border: "none", width: "100%", outline: "none", height: "100%", backgroundColor: "#ffffff"
    },
    fullWidth: {
        width: "100%"
    },
    editorTitle: {
        color: 'rgba(0,0,0,0.54)', fontSize: "20px", textTransform: "capitalize", textAlign: "left"
    },
    fNameText: {
        padding: "0px 8px", fontSize: "18px", display: "flex", justifyContent: "center", flexDirection: "column"
    }
});

class HtmlEditor extends React.Component {
    state = { edCode: '', theme: "github", hasError: false, fName: 'untitled.html', editing: false };

    componentDidMount() { this.setState({ edCode: getNewFileCode() }, () => { if (window.screen.width <= 600) this.onRotate(null); }); }

    onCodeChange = (edCode) => { this.setState({ edCode, editing: true }); }

    onThemeChange = () => {
        let theme = "monokai", headerElem = document.getElementById("headerWraId"), bodyElem = document.getElementById("edId");
        let titleElem = document.getElementById("titleId"), fNameElem = document.getElementById("fileNameId");
        if (this.state.theme === "monokai") {
            theme = "github";
            headerElem.style.backgroundColor = "#f4f4f4";
            bodyElem.style.backgroundColor = "#f4f4f4";
            titleElem.style.color = "rgba(0,0,0,0.54)";
            fNameElem.style.color = "rgba(0,0,0,0.54)";
        } else {
            bodyElem.style.backgroundColor = "rgba(106,106,106,1)";
            headerElem.style.backgroundColor = "rgba(106,106,106,1)";
            titleElem.style.color = "#ffffff";
            fNameElem.style.color = "#ffffff";
        }
        this.setState({ theme });
    }

    onRun = () => {
        document.getElementById("frameId").srcdoc = this.state.edCode;
        //need auto save and then run
        // this.setState({ editing: false });
    }

    onSave = () => { this.dialogRef.onOpen(true, this.state.fName); }

    storeHtmlContent = (fName) => {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.state.edCode));
        element.setAttribute('download', fName);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
        this.setState({ fName, editing: false });
    }

    onRotate = () => {
        let editorWrapper = document.getElementById("editorWrapperId"), editorElem = document.getElementById("editor-synopi");
        let frameElem = document.getElementById("frameWraId"), dividerElem = document.getElementById("dividerId");
        if (editorWrapper.style.display === "block") {
            editorWrapper.style.display = "flex";
            editorElem.style.height = 'calc(100vh - 86px)';
            frameElem.style.height = 'calc(100vh - 86px)';
            dividerElem.style.height = 'calc(100vh - 84px)';
            dividerElem.style.width = "8px";
            dividerElem.style.cursor = "col-resize";
        } else {
            editorWrapper.style.display = "block";
            editorElem.style.height = 'calc(50vh - 48px)';
            frameElem.style.height = 'calc(50vh - 48px)';
            dividerElem.style.height = "8px";
            dividerElem.style.width = "100%";
            dividerElem.style.cursor = "row-resize";
        }
        this.aceEditorRef.editor.resize();
    }

    onMenuClick = (item, file = {}) => {
        if (item === "save_as") this.onSave();
        else if (item === "save") this.onSave();
        else if (item === "new_file") this.openNewFile();
        else if (item === "defult_code") this.onAddDefaultCode();
        else if (item === "run") this.onRun();
        else if (item === "rotate") this.onRotate();
        else if (item === "change_theme") this.onThemeChange();
        else if (item === "open")
            this.setState({ edCode: file.code, fName: file.name }, () => {
                document.getElementById("frameId").srcdoc = '';
            });
    }

    onAddDefaultCode = () => {
        this.setState({ edCode: getDefaultCode(), fName: 'default.html', editing: false }, () => {
            document.getElementById("frameId").srcdoc = '';
        });
    }

    openNewFile = () => {
        this.setState({ edCode: getNewFileCode(), fName: 'untitled.html', editing: false }, () => {
            document.getElementById("frameId").srcdoc = '';
        });
    }

    getEditorJSX = (classes) => {
        return (<div className={classes.aceEdWra}>
            <AceEditor
                style={{ width: "100%", height: 'calc(100vh - 86px)', flex: 1 }} placeholder="start from here"
                mode="html" ref={r => this.aceEditorRef = r} theme={this.state.theme} name="editor-synopi"
                onChange={this.onCodeChange} fontSize={14} showPrintMargin={false} showGutter
                highlightActiveLine value={this.state.edCode} wrapEnabled
                setOptions={{
                    enableBasicAutocompletion: true, enableLiveAutocompletion: true, enableSnippets: false, showLineNumbers: true, tabSize: 4, useWorker: false
                }} />
        </div>);
    }

    getHeaderJSX = (classes) => {
        return (<div id="headerWraId" className={classes.headerWra} >
            <span title="Menu" onClick={(e) => this.menuRef.onMenuOpen(e.currentTarget)} className={classes.icWra}><MenuIcon className={classes.icon} /></span>
            <span title="Change Theme" onClick={this.onThemeChange} className={classes.icWra}><TonalityIcon className={classes.icon} /></span>
            <span title="Change Orientation" onClick={this.onRotate} className={classes.icWra}><ScreenRotationIcon className={classes.icon} /></span>
            <span className={classes.runBtn} onClick={this.onRun}>{"RUN >>"}</span>
            <span className={classes.fNameText} id="fileNameId">{this.state.fName + (this.state.editing ? " *" : "")}</span>
        </div>);
    }

    getFrameJSX = (classes) => {
        return (<div id="frameWraId" className={classes.frameWra}>
            <iframe className={classes.frame} id="frameId" name="frame-name" title={"iframe"} srcDoc="<p></p>" src=""></iframe>
        </div>);
    }

    render() {
        let { classes } = this.props;
        return (
            <div id="edId" className={classes.edCont}>
                <Typography variant="body2" component="div">
                    <div id="titleId" className={`${classes.editorTitle}`}>HTML Editor</div>
                    {this.getHeaderJSX(classes)}
                    <div className={classes.editorWrapper} id="editorWrapperId">
                        {this.getEditorJSX(classes)}
                        <div id="dividerId" title="Re-size" style={{ width: "8px", cursor: "col-resize" }} />
                        {this.getFrameJSX(classes)}
                    </div>
                    <DialogUI storeHtmlContent={this.storeHtmlContent} ref={dr => this.dialogRef = dr} />
                    <MenuUI ref={r => this.menuRef = r} theme={this.state.theme} onMenuClick={this.onMenuClick} />
                </Typography>
            </div>
        );
    }
}

export default withStyles(useStyles)(HtmlEditor);
