import React from 'react';
import ReactDOM from 'react-dom';
import HtmlEditor from './HtmlEditor';

ReactDOM.render( <HtmlEditor />, document.getElementById('root') );
