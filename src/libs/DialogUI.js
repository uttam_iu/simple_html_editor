import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = theme => ({
    capitalize: {
        textTransform: "capitalize"
    },
    okBtn: {
        padding: "0px 12px", marginRight: "16px", height: "32px",
    },
    respDia: {
        width: "470px",
    },
    "@media (max-width: 600px)": {
        respDia: {
            width: window.screen.width, height: window.screen.height, overflow: "hidden", margin: "0px", maxHeight: "100%", maxWidth: "100%", position: "fixed", bottom: 0, right: 0, left: 0
        },
        formWra: {
            overflowY: "auto", display: "flex", flexDirection: "column", flex: 1
        }
    }
});

class DialogUI extends React.Component {
    state = { openDialog: false, fieldValue: '' };

    onSubmit = (e) => {
        e.preventDefault();
        let fileName = this.state.fieldValue;
        if (!fileName.toLowerCase().includes(".html")) fileName = fileName + ".html";
        this.setState({ openDialog: false }, () => { this.props.storeHtmlContent(fileName); });
    }

    onChange = (e) => { this.setState({ fieldValue: e.target.value }); }

    onOpen = (openDialog = true, fieldValue = '') => { this.setState({ openDialog, fieldValue }); }

    onInput = (e) => {
        let txtElem = document.getElementById("fieldNameId"), fileName = e.target.value;
        if ((fileName.length > 5 && fileName.substring(fileName.length - 5, fileName.length).toLowerCase() === ".html"))
            fileName = fileName.substring(0, fileName.length - 5);
        if (fileName.includes(".")) {
            txtElem.setCustomValidity("you can use . only with '.html' extension.");
        } else txtElem.setCustomValidity("");
    }

    getDialogJSX = () => {
        let { classes } = this.props;
        return (<Dialog classes={{ paperWidthSm: classes.respDia }} open={true} aria-labelledby="form-dialog-title">
            <form className={classes.formWra} autoComplete="off" onSubmit={this.onSubmit} onInput={this.onInput}>
                <DialogTitle id="form-dialog-title" className={classes.capitalize}>Add File Name</DialogTitle>
                <DialogContent>
                    <TextField onChange={this.onChange} label="File Name" autoFocus value={this.state.fieldValue} fullWidth required variant="outlined" id="fieldNameId" />
                </DialogContent>
                <DialogActions style={{ marginBottom: "16px" }}>
                    <Button onClick={(e) => this.onOpen(false, this.state.fieldValue)} color="primary">Cancel</Button>
                    <Button className={classes.okBtn} variant="contained" type="submit" color="primary">Submit</Button>
                </DialogActions>
            </form>
        </Dialog>);
    }

    render() {
        return (<Typography variant="body2" component="div">{this.state.openDialog && this.getDialogJSX()}</Typography>);
    }
}

export default withStyles(useStyles)(DialogUI);