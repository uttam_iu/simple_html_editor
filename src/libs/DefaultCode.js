
const getDefaultCode = () => {
    return `<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .btn {
                height: 40px;
                width: 200px;
                font-size: 24px;
                cursor: pointer;
                color: #ffffff;
                background:rgba(0, 80, 135, 0.5);
            }
        </style>
    </head>
    <body>
        <script>
            function onBtnClick() {
                let elem = document.getElementById("textId");
                if(elem) elem.innerHTML = "hello world";
            }
        </script>

        <button class="btn" onclick="onBtnClick()">click me</button>
        <div id="textId"/>
    </body>
</html>`;
}

const getNewFileCode = () => {
    return `<!DOCTYPE html>
<html>
    <body>
    
    </body>
</html>`;
}

export { getDefaultCode, getNewFileCode };
