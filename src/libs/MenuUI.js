import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = theme => ({
    capitalize: {
        textTransform: "capitalize"
    },
    paper: {
        margin: "44px 0px 0px -8px", width: "180px"
    },
    github: {
        background: "#f4f4f4", color: "rgba(0,0,0,0.75)"
    },
    monokai: {
        background: "rgba(106,106,106,1)", color: "#FFFFFF"
    }
});

class MenuUI extends React.Component {
    state = { anchorEl: null };

    onMenuOpen = (target) => { this.setState({ anchorEl: target }); }

    onMenuChange = (item) => {
        this.onMenuOpen(null);
        if (item !== "open") this.props.onMenuClick(item);
    }

    onOpen = (e, item) => {
        let file = e.target.files[0], reader = new FileReader();
        reader.readAsText(file);
        reader.onload = (event) => {
            let fileObj = { name: file.name, code: event.target.result };
            this.props.onMenuClick(item, fileObj);
        }
        reader.onerror = () => { alert("something went wrong"); }
    }

    render() {
        let { classes } = this.props;
        return (<Typography variant="body2" component="div">
            <Menu classes={{ paper: classes.paper, list: classes[this.props.theme] }} anchorEl={this.state.anchorEl} keepMounted open={Boolean(this.state.anchorEl)} onClose={(e) => this.onMenuOpen(null)}>
                <MenuItem onClick={(e) => this.onMenuChange("new_file")}>New File</MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("open")}>
                    <input type="file" id="file-for-edcode" style={{ display: "none" }} accept=".html" onChange={(e) => this.onOpen(e, "open")} />
                    <label style={{ cursor: "pointer" }} htmlFor="file-for-edcode">{"Open File"}</label>
                </MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("run")}>Run</MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("save")}>Save</MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("save_as")}>Save As</MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("rotate")}>Rotate</MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("change_theme")}>Change Theme</MenuItem>
                <MenuItem onClick={(e) => this.onMenuChange("defult_code")}>Add Default Code</MenuItem>
            </Menu>
        </Typography>);
    }
}

export default withStyles(useStyles)(MenuUI);